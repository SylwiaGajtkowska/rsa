#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <cstdlib>
#include <windows.h>

using namespace std;

long long ImMod(long long a, long long b);
long long NWD(long long a, long long b);
long long powerModulo(long long number, long long power, long long modNumber);
bool liczbaPierwsza(long long n);

int main()
{
	long long p, q, n, e, d, fi, count;

	/*cout << "Podaj e: ";
	cin >> e;*/
	//e = (long long)(rand() % RAND_MAX);

	//d*e = 1 mod(fi(n))
	//e = 1 mod(fi(n))/d
	e = 3;

	string wiadomosc = "";
	
	//Kontener na dane zaszyfrowane i odszyfrowane
	vector<long long>zaszyfrowane;
	vector<long long>zdeszyfrowane;

	long long var = 0;
	int tmp = 0;

	//d = e ^-1
	d = -1;
	count = 0;

	cout << "Podaj p: ";
	cin >> p;
	cout << "Podaj q: ";
	cin >> q;
	//p = (long long)(rand() % RAND_MAX);
	//q = (long long)(rand() % RAND_MAX);

	n = p * q;
	if (liczbaPierwsza(p) == true)
	{
		if (liczbaPierwsza(q) == true)
		{
			if (int(n)>256)
			{
				fi = (p - 1)*(q - 1);
				while (d == -1)
				{
					e += 2;
					if (NWD(e, fi) == 1)
					{
						d = ImMod(e, fi);
						if (d == -1) continue;
						else
						{
							break;
						}
					}
					else continue;
					count++;
				}

				cout << "Wiadomosc do zaszyfrowania: " << endl;
				cin >> wiadomosc;

				cout << endl << "Wiadomosc w ASCII : ";
				for (long long i = 0; i<wiadomosc.size(); i++)
				{
					cout << (int)wiadomosc[i] << " ";
				}

				cout << endl << "Tekst zaszyfrowany: ";
				for (long long i = 0; i<wiadomosc.size(); i++)
				{
					tmp = (int)wiadomosc[i];
					var = powerModulo(tmp, e, n);
					zaszyfrowane.push_back(var);
					cout << zaszyfrowane.at(i) << " ";
				}

				cout << endl << "Tekst zdeszyfrowany w ASCII: ";

				for (long long i = 0; i<zaszyfrowane.size(); i++)
				{
					tmp = (int)zaszyfrowane[i];
					var = powerModulo(tmp, d, n);
					zdeszyfrowane.push_back(var);
					cout << zdeszyfrowane.at(i) << " ";
				}

				cout << endl << "Tekst zdeszyfrowany : ";

				for (long long i = 0; i<zaszyfrowane.size(); i++)
				{
					tmp = (int)zaszyfrowane[i];
					var = powerModulo(tmp, d, n);
					zdeszyfrowane.push_back(var);
					cout << (char)zdeszyfrowane.at(i) << " ";
				}

			}
			else return -1;
		}
		else return -1;
	}
	else return -1;
	cout << "Koniec..." << endl;

	system("PAUSE");
	return 0;
}

long long ImMod(long long a, long long b) {

	vector<int> s;
	vector<int> q;
	vector<int> r;

	bool check = true;
	long long count = 2;

	q.push_back(0);
	q.push_back(0);

	if (a>b)
	{
		r.push_back(a);
		r.push_back(b);
		s.push_back(1);
		s.push_back(0);
	}
	else {
		r.push_back(b);
		r.push_back(a);
		s.push_back(0);
		s.push_back(1);
	}

	do {
		q.push_back(r.at(count - 2) / r.at(count - 1));
		r.push_back(r.at(count - 2) - (q.at(count)*r.at(count - 1)));
		s.push_back(s.at(count - 2) - (q.at(count)*s.at(count - 1)));

		if (r.at(count) == 1)
		{
			return s.at(count);
		}
		else if (r.at(count) == 0)
		{
			return -1;
		}
		count++;

	} while (check == true);

}

long long NWD(long long a, long long b) {

	while (a != b)
		if (a>b)
			a -= b;
		else
			b -= a;
	return a;
}

long long powerModulo(long long number, long long power, long long modNumber) {
	long long tempPower = power;
	string pow;

	while (tempPower != 0) {
		long long help = tempPower % 2;
		pow.push_back(help);
		tempPower /= 2;
	}

	long long *tab;
	tab = new long long[pow.length()];
	tab[0] = number % modNumber;
	for (long long i = 1; i < pow.length(); i++) {
		tab[i] = (tab[i - 1] * tab[i - 1]) % modNumber;

	}

	long long finalAnswer = 1;
	for (long long i = 0; i < pow.length(); i++) {
		if (pow[i] == 1) {
			finalAnswer = (finalAnswer*tab[i]) % modNumber;
		}
	}
	return finalAnswer;
}

bool liczbaPierwsza(long long n) {
	unsigned long r = sqrt(n);
	unsigned long i = 2;

	while (i <= r)
	{
		if (!(n % i++)) return false;
	}
	return true;
}